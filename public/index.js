var app = require("express")()
var bodyParser = require("body-parser")
var upload = require('multer')();

var curaEngineHandler = require("./../handler/curaEngine")
var rootHandler = require("./../handler/root")
var healthzHandler = require("./../handler/healthz")

const PORT = 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.urlencoded({ extended: true }))

app.post("/calc", upload.array(), curaEngineHandler)
app.get("/", rootHandler)
app.get("/healthz", healthzHandler)

app.listen(PORT, function() {
    console.log("listening on port ", PORT)
})
